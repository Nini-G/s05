let num_sum = 5000;
let numSum = 6000;

console.log(num_sum);
console.log(numSum);

//Declaring multiple variable
let brand = "Toyota", model = "Vios", type = "Sedan";
console.log(brand);
console.log(model);
console.log(type);

//console logging multiple variables: use comma to separate variables
console.log(brand, model, type);

/*
    Data Types
        In most programming language, data is differentiated by their types.

        For most programming languages, you have to declare not the variable only but also the type of data saving into a variable.
        However, JS does not require this.

        To create data with data types, some data types require adding with literal.

            string literals = "", '', and most recently ``(template literal/back tick)
            object literals = {}
            array literals = []
*/
/*
String
    Strings are series of Alphanumeric that create a word, a phrase, a name or anything related to creating text.
    String literals such as '' (single quote) or ""(double quote) are used to write or to create strings.
    
*/
let country = 'Philippines';
let province = "Metro Manila";
console.log(country);
console.log(province);

let firstName = 'Juggy', lastName = "Jones";
console.log(firstName, lastName);

//Concatenation is process/operation wherein we combine two strings at once. 
//JS Strings, spaces are counted as characters. 
console.log(firstName + " " + lastName);

/*
Template literal (``) will alow uis to create string with the use of backticks. Template literals also allow us to easily concatenate strings with the use of plus signs. 

This also allows to embed or add variable and even expression in our string with the use of placeholders. ${}
*/
let fullName2 = `${firstName} ${lastName}`;
console.log(fullName2);

/*
Number (data type)
integers (whole number) and float(decimal numbers)
 */
let numString1 = '5';
let numString2 = "6";
let num1 = 5;
let num2 = 6;


console.log(numString1 + numString2); //56 because concatenation
console.log(num1 + num2); // 11 because boith integers

let num3 = 5.5;
let num4 = .5;

console.log(num1 + num3);
console.log(num3 + num4); 

//+ concatenation for strings
// + both numbers will add.

console.log(numString1 + num2); // 55 resulted to concatenation
console.log(num3 + numString2) //if one string and one integer, will only concatenate.
//forced coercion - when one data type is forced to change to complete the operation.
// string + number = concatenation.
//we forced number to change into string.


//parseInt method can change the ype of numeric string to a proper number.
console.log(parseInt(numString1) + num1); //10 because numstring1 was parsed into a proper number.
// parseDouble() - for decimal 

//Mathematical operations (-, *, /, %)
//Subtraction
console.log("MAth*****************************************")
console.log(num1 - num3); //0.5 results in proper mathematical operation
console.log(numString1 - num2); //-1 property results in proper math operations. because the string was forced already to become a number. 
console.log(numString2 - num2); // 0
console.log(numString1 - numString2);//-1 In subtraction, numeric string will not concatenate and instead will be forcibly change its type and subtract is properly.

let sample2 = "Juan Dela Cruz";
console.log(sample2 - numString1); // NaN - result means not a number. When trying to perform subraction between the aplhanumeric string and numeric string, the result is NaN.


	/*
		Mini-Activity

		Create a variable called sentence.
		Combine the variables to form a single string which would ligibly and understandably create a sentence that you are a student of DLSUD
		Log the sentence variable in the console.
		    let word1 = "is";
			let word2 = "student";
			let word3 = "of";
			let word4 = "University";
			let word5 = " De La Salle";
			let word6 = "a";
			let word7 = "Dasmariñas";
			let space = "";
		Use string literals and template literals for 15 minutes.
		SS you codebased and your console paste via Teams chat box.
		 	Break time 30 minutes
		 	Resume at 3:50PM

	 */
let my_name = "Majo Gamit";
let word1 = "is";
let word2 = "student";
let word3 = "of";
let word4 = "University";
let word5 = " De La Salle";
let word6 = "a";
let word7 = "Dasmariñas";
let space = " ";

let sentence1 = `${my_name} ${word1} ${word6} ${word2} ${word3} ${word5} ${word4} ${word7}`;

let sentence2 = my_name + space +  word1 + space + word6 + space + word2 + space+ word3 + space + word5 + space + word4 + space + word7;

console.log(sentence1);
console.log(sentence2);

// Resumption
//Multiplication

console.log(num1 * num2);
console.log(numString1 * num1); //multiplication coerce strings into numbers
console.log(numString1 * numString2);
console.log(sample2 * 2); //strings cannot be multiplied

// let product = num1 * num2;
// console.log(product);

// //Division
// console.log(product/num2);
// console.log(numString1 / numString2);
// console.log(numString1 % numString2);


// //division / multiplication by 0
// console.log(product * 0); //0
// console.log(product / 0); // Infinity (undefined)

//Boolean (true/false)
/*
    Booelan is usually used for logic operation or if-else conditions.
        - Boolean values are normally used to store values relating to the state of  certain things.
    When creating a variable, which will contain a boolean, the variable name is usually a yes or no question.
    
*/

let isAdmin = true;
let isMarried = false;
let isMVP = true;

//You can also concatenate strings and booeland variables.
console.log("Is she married? " + isMarried);
console.log("Is he the MVP? " + isMVP);
console.log(`Is he the current Admin ${isAdmin}`);

/*
Array
    - are special kind of data type to store multiple values.
    - can actually store data with different types BUT as the best practice, arrays are used to contain multiple values of the same type.
    - values in arrays are separated by commas
    - array is created with an array literal = []
    Syntax:
        let /const arrayName = [elementA, elementB, elementC, ...];
*/

let array1 = ['Goku', 'Picolo', 'Gohan', 'Vegeta'];
console.log(array1);

let array2 = ["One Punch Man", true, 500, "Saitama"];
console.log(array2); //not advisable

/*
Objects
    - are another special kind of data type used to mimic the real world.
    - used to create a complex data that contain pieces of information that are relevant to each other.
    -Objects are created with object literal = {}
        - each data/value are paired with a key.
        - each field is called a property
        - each field is separated bu comma
    Syntax
    let/const obectName = {
        propertyA: value,
        propertyB: value, 
    }
*/

let person = {
    fullName: "Juan Dela Cruz",
    age: 35,
    isMarried: false,
    contact: [09159629338, 09568776345],
    address: {
        houseNumber: '555',
        street: 'Diamond',
        city: 'Manila',
    }
};

console.log(person);

/*Mini-Activity - 10 mins 
	Create a variable with a group of data
		- The group of data should contain names from your favorite band/idol

	Create a variable which contain multiple values of differing types and describes a single person.
		- This data type should be able to contain multiple key value pairs:
			firstName: <value>
			lastName: <value>
			isDeveloper: <value>
			hasPortfolio: <value>
			age: <value>
			contact: <value>
			address:
				houseNumber: <value>
				street: <value>
				city: <value>
*/

let favoriteIdols = ["Taylor Swift", "BLACKPINK", "iKON", "Ed Sheeran", "NIKI", "The Weeknd"]

let personID = {
    firstName: "Jill", 
    lastName: "Barber", 
    isDeveloper: false, 
    hasPortfolio: true, 
    age: 57, 
    contact: [09159629338, 09874467634], 
    address: {
        houseNumber: 327, 
        street: "Forbes Park", 
        city: "Makati", 
    }
};

console.log(favoriteIdols);
console.log(personID);

//Undefine vs Null
/*
    NULL - is explicit absence of data/value. This is done to preject that a variable contains nothing over undefined as undefined merely means there is no data in the variable BECAUSE the variable has not been assigned an initial value. 

*/


let foundResult = null;

let myNumber = 0;
let myString =  " ";

//usin null to compare to a 0 value and an empty string is much better for readability.
console.log(foundResult);


//undefined, variable exists, but value in unknown, it is not associated.
let person2 = {
    name: "Peter",
    age: 25
};

console.log(person2.name);
//becaseu person 2 exists, but the property isAdmin does not. 
console.log(person2.isAdmin);

/*
    FUNCTIONS
        - in JS are line/block of codes that tell our device or application to perform a certain task when called/invoked
        - are reusable pieces of code with instruction which used over and over again just as long as we can call invoke them.

    Syntax:
        fucntion functionName() {
            code block
                - the block of code that will be executed once the function has beeen run/called/invoked.
        }

*/
function printName() {
    console.log("My name is Jin.");
};

//invoke/calling the fucntion 
printName();
printName();
printName();

function showSum() {
    console.log(25 + 6);
};

showSum();

/*
    Parameters and Arguments
        - a parameter acts as a named varaible/container that exists only inside a function
        -THis is used as to store information/to act as a stand-in or the continue valued passed into the function as an argument.

        -data passed into the function is called the argument.
        -representation of argumetn inside the function, is called parameter.
*/
function printName1(name) {
    console.log(`My name is ${name}.`)
}

printName1("Rosie");

function displayNum(number) {
    alert(number);
};

// displayNum(13);


function showMessage(language) {
    console.log(`${language} is Fun!`);
};

showMessage("JavaScript");
showMessage("C++");

//multiple parameters and
function displayFullName (fistName, mi, lastName, age){
    console.log(`${fistName} ${mi} ${lastName} is ${age} years old`);
}
displayFullName("Majo", "R.", "Gamit", 19)

function createFullName (fistName, mi, lastName){
    return `${fistName} ${mi} ${lastName}`;
    console.log("Program finished.");
}

let fullName1 = createFullName("Jennie", "R.", "Kim");
let fullName5 = createFullName("Taylor", "A.", "Swift");
console.log(fullName1);
console.log(fullName5);



/* Assignmment - Instructions:

	Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.

	Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.

	Create function which will be able to multiply two numbers.
		-Numbers must be provided as arguments.
		-Return the result of the multiplication.

	-Create a new variable called product.
		-This product should be able to receive the result of multiplication function.

	Log the value of product variable in the console. */
    
    
    
function Add(num1, num2) {
    console.log(num1 + num2);
}

function Subtract(num1, num2) {
    console.log(num1 - num2);
}

function Multiply(num1, num2) {
    return num1 * num2;
}

Add(13, 7);
Subtract(57, 6)
let product = Multiply(3, 7);
console.log(product);